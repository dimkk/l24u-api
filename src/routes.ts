/**
 * Main application routes
 */

import * as err from "./common/Error";
import * as Configs from "./configurations";
let config = Configs.getServerConfigs();

export default function(app) {
  // Insert routes below
  app.use('/api/things', require('./api/thing'));
  app.use('/api/users', require('./api/user'));
  app.use('/api/invite', require('./api/invite'));
  app.use('/api/history', require('./api/history'));

  app.use('/auth', require('./auth').default);
  app.get('/rapi/*', require('./proxy'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(err.pageNotFound);

  // All other routes should redirect to the index.html
    //   app.route('/*')
    //     .get((req, res) => {
    //       res.sendFile(path.resolve(`${app.get('appPath')}/index.html`));
    //     });
}
