import * as Cfg from '../configurations';
let config = Cfg.getServerConfigs();
var email   = require("emailjs/email");
import * as q from 'q';
import { IUser } from '../api/user/IUser';
import { IInvite } from '../api/invite/iinvite';

export class Mailer {

    static server  = email.server.connect({
        user:    config.mailUser,
        password:config.mailPass,
        host:    config.mailHost,
        ssl:     true
    });

    static commonFrom = '"admin 👥" <lineage2foru@yandex.ru>';

    public static sendInvite(user: IUser, invite: IInvite) {
        let def = q.defer();
        let email = encodeURIComponent(invite.inviteEmail);
        let html =  `Привет от сервера L24u.ru! Тебя пригласили на закрытый тест нашего свежего сервера на базе Lineage 2 HF х10 <br />
            Тебя пригласил ${user.name} ${ invite.inviteMessage ? 'и написал следующее - ' + invite.inviteMessage : ''} <br />
            <a href="https://landing.l24u.ru">Тут</a> можно посмотреть о чём сервер, <a href="https://l24u.ru">Тут</a> пока форум <br />
            А <a href="https://lk.l24u.ru/#/register?code=${invite.code}&email=${email}">здесь</a> ты сможешь использовать свой инвайт, зарегистрироваться, скачать файлы и играть!<br/>
            Код для регистрации - ${invite.code}<br/>
            <br />
            <br />
            ---<br />
            С Уважением,<br />
            Команда <a href="https://l24u.ru">L24u.ru</a>
            `;
        this.sendCommonMail(
            html,
            this.commonFrom,
            invite.inviteEmail,
            'Приглашение на закрытый тест сервера L24u.ru!'
        ).then((msg) => {
            def.resolve(msg);
        }).catch((err) => {
            def.reject(err);
        });
        return def.promise;
    }

    public static sendCommonMail(html:string, from:string, to:string, subject:string, cc?) {
        let def = q.defer();
        let opts = {
            text: 'test',
            from: from,
            to: to,
            subject: subject,
            attachment: [
                    {data: html, alternative:true}
             ]
        };
        this.server.send(opts,
            function(error, message) {
                if (error) {
                    def.reject(error);
                }
                console.log('Message sent: ' + message);
                def.resolve(message);
            });
        return def.promise;
    }
}

