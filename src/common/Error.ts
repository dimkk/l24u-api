/**
 * Error responses
 */

'use strict';

export function pageNotFound(req, res) {
  var viewFilePath = '404';
  var statusCode = 404;
  var result = {
    status: statusCode
  };

  res.status(result.status);
  res.render(viewFilePath, {}, function(err, html) {
    if(err) {
      return res.status(result.status).json(result);
    }

    res.send(html);
  });
};

export function errorHandler (err, req, res, next) {
  if (process.env.NODE_ENV === "dev") {
    console.log(err.stack);
    if (res.headersSent) {
      return next(err);
    }
    res.status(500);
    res.send({ error: err });
  } else {
    console.log(err.stack);
    res.status(500);
    res.send('Omg, smth broke(');
  }
  
  //next(err);
}


