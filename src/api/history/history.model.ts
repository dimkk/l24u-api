'use strict';

import * as mongoose from 'mongoose';
import { IHistory } from './ihistory';

var HistorySchema = new mongoose.Schema({
    message: String,
    //game | site 
    location: String,
    //error | warning | info | success
    type: String,
    subjectId: String
});

export default mongoose.model<IHistory>('History', HistorySchema);
