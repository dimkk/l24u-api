import { Document } from 'mongoose';

export interface IHistory extends Document {
  message: string;
  //game | site 
  location: string;
  //error | warning | info | success
  type: string;
  subjectId: string;
}