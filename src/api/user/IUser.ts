import { Document } from 'mongoose';
import * as q from 'q';

export interface IUser extends Document {
  l2Id?: number;
  invitesLeft: number;
  money: number;
  loginFailedAttempts;
  invitedBy: string;
  name?: string;
  email?: IEmail | string;
  role?: string;
  password?: string;
  salt?: string;
  facebook?: any;
  vkontakte?: any;
  provider?: string;
  profile?: IUserProfile;
  authenticate(pass:string);
  setInviteCount(user:IUser, count:'add' | 'substract'):q.IPromise<IUser>;
}

export interface IEmail {
  type: string;
  lowercase: boolean;
  required(): boolean;
}

export interface IUserProfile {
  name: string;
  role: string;
}