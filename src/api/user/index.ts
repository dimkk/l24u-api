'use strict';

import { Router } from 'express';
import * as controller from './user.controller';
import * as auth from '../../auth/auth.service';
import * as Cfg from '../../configurations';
let config = Cfg.getServerConfigs();
var ReCaptchaMiddleware = require('express-recaptcha-middleware')(config.recaptchaSecret);

var router = Router();
router.get('/sync', auth.hasRole('admin'), controller.syncWithDb);
router.get('/', auth.hasRole('admin'), controller.index);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/me', auth.isAuthenticated(true), controller.me);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/:id', auth.isAuthenticated(), controller.update);
router.post('/',  controller.create); //ReCaptchaMiddleware('recaptcha'),




module.exports = router;
