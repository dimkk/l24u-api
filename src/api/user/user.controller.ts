'use strict';

import User from './user.model';

import * as jwt from 'jsonwebtoken';
import { IUser, IEmail, IUserProfile } from "./IUser";
import * as Cfg from '../../configurations';
let config = Cfg.getServerConfigs();
import { Accounts, IAccount } from '../../communication/accounts';
import { IInvite } from '../invite/iinvite';
import * as q from 'q';
import * as express from 'express';
import * as cache from 'memory-cache';
let reCAPTCHA = require('recaptcha2');
import Invite from '../invite/invite.model';

let recaptcha = new reCAPTCHA({
  siteKey: config.recaptchaSite,
  secretKey: config.recaptchaSecret
});

function validationError(res, statusCode?) {
  statusCode = statusCode || 422;
  return function(err) {
    return res.status(statusCode).json(err);
  };
}

function handleError(res, statusCode?) {
  statusCode = statusCode || 500;
  return function(err) {
    return res.status(statusCode).send(err);
  };
}

/**
 * Get list of users
 * restriction: 'admin'
 */
export function syncWithDb(req, res) {
  let promises = [];
  Accounts.getAccounts().then((accs:IAccount[]) => {
    if (accs && accs.length) {
      accs.forEach((item:IAccount) => {
        let def = q.defer();
        User.findOne({ name: item.login }).exec()
        .then(user => {
            if (!user) {
                let newUser = new User;
                newUser.name = item.login;
                newUser.password = item.password;
                newUser.email = item.login + '@alfa.test';
                newUser.provider = 'local';
                newUser.role = item.login === 'dimkk' ? 'admin' : 'user';
                newUser.l2Id = item.id;
                newUser.save()
                  .then(function(user) {
                    def.resolve(user);
                  })
                  .catch((error) => {
                    def.reject(error);
                  });
            };
            def.resolve();
        })
        .catch(function (err) {
          def.reject(err);
            throw err;
        });
        promises.push(def.promise);
      });
    }
    return q.all(promises).then(() => {
      return res.status(200).send('ok');
    }).catch(handleError(res));
  });
}

/**
 * Get list of users
 * restriction: 'admin'
 */
export function index(req, res) {
  return User.find({}, '-salt').exec()
    .then(users => {
      res.status(200).json(users);
    })
    .catch(handleError(res));
}

/**
 * Creates a new user
 */
export function create(req:express.Request, res:express.Response) {
  var newUser = new User(req.body);
  newUser.provider = 'local';
  newUser.role = 'user';
  let token;
  let def = q.defer();
  if (config.useInviteToRegister) {
    if (!req.body.inviteCode) {
        return res.status(500).send('no invite code found');
    }
    recaptcha.validate(req.body.recaptcha)
        .then(function(){
          def.resolve();
        })
        .catch(function(errorCodes){
          def.reject(recaptcha.translateErrors(errorCodes)); // translate error codes to human readable text
        });
    } else {
      def.resolve();
    }
  if (config.useInviteToRegister) {
    def.promise
    .then(() => {
      return Invite.findOne({code: req.body.inviteCode});
    })
    .then((inv:IInvite) => {
      if (!inv) {
        res.status(404).send('no invite with this code found');
      }
      newUser.invitedBy = inv.inviteeId;
      inv.inviteState = 'used';
      inv.save();
      return newUser.save();
    })
    .then((user:any) => {
      token = jwt.sign({ _id: user._id }, config.jwtSecret, {
        expiresIn: 60 * 60 * 5
      });
      return Accounts.createAccount(newUser, req.ip);
    })
    .then(() => {
        res.json({ token });
    })
    .catch(validationError(res));
  } else {
    newUser.save()
      .then(function(user) {
      token = jwt.sign({ _id: user._id }, config.jwtSecret, {
        expiresIn: 60 * 60 * 5
      });
        return Accounts.createAccount(newUser, req.ip);
      })
      .then(() => {
          res.json({ token });
      })
      .catch(validationError(res));
  }
}

/**
 * Get a single user
 */
export function show(req, res, next) {
  var userId = req.params.id;

  return User.findById(userId).exec()
    .then(user => {
      if (!user) {
        return res.status(404).end();
      }
      res.json(user.profile);
    })
    .catch(err => next(err));
}

/**
 * Deletes a user
 * restriction: 'admin'
 */
export function destroy(req, res) {
  return User.findByIdAndRemove(req.params.id).exec()
    .then(function() {
      res.status(200).end();
    })
    .catch(handleError(res));
}

/**
 * Change a users password
 */
export function changePassword(req, res) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  return User.findById(userId).exec()
    .then(user => {
      if (user.authenticate(oldPass)) {
        user.password = newPass;
        let id = user.l2Id;
        return user.save()
          .then(() => {
            if (oldPass !== newPass) {
              return Accounts.patchAccount({password:newPass}, id);
            } else {
              res.status(200).end();
            }
          })
          .then((data) => {
                res.status(200).end();
          })
          .catch(validationError(res));
      } else {
        return res.status(403).end();
      }
    });
}

export function update(req, res) {
  var userId = req.body._id;
  let newUser:IUser = req.body;
  let id = newUser.l2Id;
  let oldPasss, newPass = newUser.password;
  return User.findById(userId).exec()
    .then(user => {
      oldPasss = user.password;
      if (user._id) {
        delete user._id;
      }
      return User.findOneAndUpdate({_id: req.params.id}, newUser).exec()
      })
    .then(() => {
      if (id && oldPasss !== newPass) {
        return Accounts.patchAccount({password:newUser.password}, id);
      } else {
        res.status(200).end();
      }
    })
    .then((data) => {
          res.status(200).end();
    })
    .catch(validationError(res));
}

/**
 * Get my info
 */
export function me(req, res, next) {
  var userId = req.user._id;

  return User.findOne({ _id: userId }, '-salt').exec()
    .then(user => { // don't ever give out the password or salt
      if (!user) {
        return res.status(401).end();
      }
      res.json(user);
    })
    .catch(err => next(err));
}

/**
 * Authentication callback
 */
export function authCallback(req, res) {
  res.redirect('/');
}
