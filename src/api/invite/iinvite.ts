import { Document } from 'mongoose';
import * as q from 'q';
import { IUser } from '../user/IUser';

export interface IInvite extends Document {
    code?: string;
    inviteeId: string;
    inviteEmail?: string;
    inviteMessage?:string;
    getInvitee(inv: IInvite, checkInvitesLeft):q.Promise<IUser>;
    inviteState: 'new' | 'sent' | 'used' | 'expired';
}