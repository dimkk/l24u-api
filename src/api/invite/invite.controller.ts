/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/Invites              ->  index
 * POST    /api/Invites              ->  create
 * GET     /api/Invites/:id          ->  show
 * PUT     /api/Invites/:id          ->  upsert
 * PATCH   /api/Invites/:id          ->  patch
 * DELETE  /api/Invites/:id          ->  destroy
 */


let jsonpatch = require('fast-json-patch');
import Invite from './invite.model';
import User from '../user/user.model';
import { IInvite } from "./iinvite";
import { IUser } from '../user/IUser';
import { Mailer } from '../../common/mailer';
import * as q from 'q';
const uuidV4 = require('uuid/v4');
import { Request } from 'express';

function respondWithResult(res, statusCode?) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(200).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode?) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Invites
export function index(req:Request, res) {
  let uid = req.query.id;
  return Invite.find({'inviteeId':uid}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Invite from the DB
export function show(req, res) {
  return Invite.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Invite in the DB
export function create(req, res) {
  let inv:IInvite = new Invite(req.body);
  inv.getInvitee(inv, true)
    .then((user: IUser) => {
      return user.setInviteCount(user, 'substract');
    })
    .then((user) => {
      inv.code = uuidV4();
      return Invite.create(inv);
    })
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given Invite in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Invite.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Upserts the given Invite in the DB at the specified ID
export function send(req, res) {
  let iinvite:IInvite;
  Invite.findById(req.body._id)
    .then((invite: IInvite ) => {
      iinvite = invite;
      return invite.getInvitee(invite, false)
    })
    .then((user) => {
      iinvite.inviteState = 'sent';
      return Mailer.sendInvite(user, iinvite);
    })
    .then(() => {
      if (iinvite._id) {
        delete iinvite._id;
      }
      console.log(iinvite.inviteState);
      return Invite.findOneAndUpdate({_id: req.params.id}, iinvite, {new: true}).exec();
    })
    .then((invite:IInvite | any) => {
      console.log(invite.inviteState);
      console.log(invite._doc.inviteState);
      if (invite) {
        return res.status(200).json(invite);
      }
      return null;
    })
    .catch(handleError(res));
}

// Updates an existing Invite in the DB
export function patch(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Invite.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Invite from the DB
export function destroy(req, res) {
  return Invite.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
