'use strict';

import * as mongoose from 'mongoose';
import { IInvite } from './iinvite';
import * as q from 'q';
import User from '../user/user.model';
import { IUser } from '../user/IUser';

var InviteSchema = new mongoose.Schema({
    code: String,
    inviteeId: { type: String, index:true },
    inviteEmail: String,
    inviteMessage:String,
    inviteState: { type: String, default: 'new' }
});

InviteSchema.methods = {
    getInvitee(inv: IInvite, checkInvitesLeft):q.IPromise<IUser> {
        let def = q.defer();
        User.findById(inv.inviteeId).then((user: IUser) => {
            if (checkInvitesLeft) {
                if (user.invitesLeft > 0) {
                def.resolve(user);
                } else {
                    def.reject(false);
                }
            } else {
                def.resolve(user);
            }
        }).catch(() => {
            def.reject(false);
        });
        return def.promise;
    }
};

export default mongoose.model<IInvite>('Invite', InviteSchema);
