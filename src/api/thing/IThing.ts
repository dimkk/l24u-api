import { Document } from 'mongoose';

export interface IThing extends Document {
    name: string;
    info: string;
    active: boolean;
}