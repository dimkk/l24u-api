'use strict';

import * as mongoose from 'mongoose';
import { IThing } from './IThing';

var ThingSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean
});

export default mongoose.model<IThing>('Thing', ThingSchema);
