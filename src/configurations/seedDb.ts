/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

import Thing from '../api/thing/thing.model';
import  User from '../api/user/user.model';

export function doSeed() {
    User.create({
      provider: 'local',
      name: 'superadmin',
      email: 'd-volkovsky@yandex.ru',
      password: 'Mallory122907',
      role: 'admin'
    })
    .then(() => {
      console.log('finished populating superadmin');
    }).catch(() => {

    });
};

