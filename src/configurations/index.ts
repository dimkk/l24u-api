import * as nconf from "nconf";
import * as path from "path";

//Read Configurations
// const configs = new nconf.Provider({
//   env: {separator:'__'},
//   argv: true,
//   store: {
//     type: 'file',
//     file: path.join(__dirname, `./config.${process.env.NODE_ENV || "dev"}.json`)
//   }
// });

let confFileName = path.join(__dirname, `./config.${process.env.NODE_ENV || "dev"}.json`);
let good = `../src/configurations/config.${process.env.NODE_ENV || "dev"}.json`;

nconf
    .env({separator: '__'})
    .argv()
    .file(confFileName)
    ;

export interface IMqConfiguration {
    RABBITMQ_ADDRESS: string;
    RABBITMQ_LOGIN: string;
    RABBITMQ_PASS: string;
    RABBITMQ_QUEUE: string;
}

export interface IServerConfigurations {
    port: number;
    jwtSecret: string;
    jwtExpiration: string;
    mongo: IMongoConfiguration;
    seedDb: boolean;
    userRoles: string[];
    recaptchaSecret:string;
    recaptchaSite:string;
    mailUser:string;
    mailPass:string;
    mailHost:string;
    proxyAddress:string;
    maxFailLoginTries: number;
    failLoginTriesSaveTime: number;
    useInviteToRegister:boolean;
}

export interface ISocialCfg {
    DOMAIN: string;

    FACEBOOK_ID: string;
    FACEBOOK_SECRET: string;
    FACEBOOK_CB: string;

    VK_ID: string;
    VK_SECRET: string;
    VK_CB: string;
}

export interface IDb {
    baseApi:string;
    email:string;
    pass:string;
}

export interface IMongoConfiguration {
    uri: string;
    options:any;
}

export function getServerConfigs(): IServerConfigurations {
    return nconf.get("server");
}

export function getSocialCfg(): ISocialCfg {
    return nconf.get("social");
}

export function getCfg() {
    return nconf;
};