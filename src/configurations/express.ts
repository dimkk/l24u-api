/**
 * Express configuration
 */

'use strict';

import * as express from 'express';
let bodyParser = require('body-parser');
let methodOverride = require('method-override');
let cookieParser = require('cookie-parser');
import * as path from 'path';
let lusca = require('lusca');
import * as Cfg from './index';
import * as passport from 'passport';
import * as session from 'express-session';
import * as connectMongo from 'connect-mongo';
import * as mongoose from 'mongoose';


var MongoStore = connectMongo(session);
let config = Cfg.getServerConfigs();
export default function(app) {
  app.set('views', `.src/views`);
  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');

  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(methodOverride());
  app.use(cookieParser());
  app.use(passport.initialize());


  // Persist sessions with MongoStore / sequelizeStore
  // We need to enable sessions for passport-twitter because it's an
  // oauth 1.0 strategy, and Lusca depends on sessions
  app.use(session({
    secret: config.jwtSecret,
    saveUninitialized: true,
    resave: false,
    store: new MongoStore({
      mongooseConnection: mongoose.connection
    })
  }));

  

  if (process.env.NODE_ENV === "prod") {
  /**
     * Lusca - express server security
     * https://github.com/krakenjs/lusca
     */
    app.use(lusca({
        // csrf: {
        //   angular: true
        // },
        xframe: 'SAMEORIGIN',
        hsts: {
          maxAge: 31536000, //1 year, in seconds
          includeSubDomains: true,
          preload: true
        },
        xssProtection: true
      }));
  }
}
