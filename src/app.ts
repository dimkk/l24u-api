import * as express from 'express';
import * as mongoose from 'mongoose';
(<any>mongoose).Promise = require('bluebird');
import { IServerConfigurations } from "./configurations";
import * as http from 'http';
import * as seedDb from "./configurations/seedDb";
import { errorHandler } from "./common/Error";
import * as cors from 'cors';
import * as Configs from "./configurations";
import { Accounts } from './communication/accounts';


let config:IServerConfigurations = Configs.getServerConfigs();
// Connect to MongoDB
console.log('connecting to mongo here ' + config.mongo.uri);
mongoose.connect(config.mongo.uri, config.mongo.options);
mongoose.connection.on('error', function(err) {
  console.error(`MongoDB connection error: ${err}`);
  process.exit(-1); // eslint-disable-line no-process-exit
});

// Populate databases with sample data
if (config.seedDb) {
  require('./configurations/seedDb').doSeed();
}

// Setup server
var app = express();

var corsOptions = {
            origin: process.env.ORIGIN || 'http://localhost:3000', //'http://127.0.0.1:8080',
            optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
            credentials: true
    };
app.use(cors(corsOptions));

var server = http.createServer(app);
require('./configurations/express').default(app);
require('./routes').default(app);

app.use(errorHandler);



// Start server
app.listen(config.port, function() {
    console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
 });

process.on('uncaughtException', function(error) {
  //errorHandler(error)
  console.log('ex');
});

// Expose app
exports = module.exports = app;