'use strict';
import * as express from 'express';
import * as Cfg from '../configurations';
import User from '../api/user/user.model';
let config = Cfg.getSocialCfg();

// Passport Configuration
require('./local/passport').setup(User, config);
require('./facebook/passport').setup(User, config);
require('./vk/passport').setup(User, config);

var router = express.Router();

router.use('/local', require('./local').default);
router.use('/facebook', require('./facebook').default);
router.use('/vk', require('./vk').default);

export default router;
