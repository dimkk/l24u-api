import User from '../../api/user/user.model';
var nodemailer = require('nodemailer');

import * as jwt from 'jsonwebtoken';
import { IUser, IEmail, IUserProfile } from "../../api/user/IUser";
import * as Cfg from '../../configurations';
let config = Cfg.getServerConfigs();

var email   = require("emailjs/email");
var server  = email.server.connect({
   user:    config.mailUser,
   password:config.mailPass,
   host:    config.mailHost,
   ssl:     true
});

export function restore (req, res, next) {
    if (!req.body.email) {
        res.status(500).send("Почта не указана");
        return;
    };
    let value = req.body.email;
    User.findOne({ email: value }).exec()
        .then(user => {
            if (!user) {
                res.status(500).send("Пользователь не найден");
                return;
            } ;
            server.send({
            text:    'Привет ' + user.name + ' 🐴 ! Пароль - ' + user.password,
            from:    '"admin 👥" <lineage2foru@yandex.ru>',
            to:      user.email,
            subject: 'L24u pass recovery', },
            function(error, message) {
                if (error) {
                    res.status(500).send(error);
                    return;
                }
                console.log('Message sent: ' + message);
                res.status(200).send(message);

            });
        })
        .catch(function (err) {
            throw err;
        });
}
