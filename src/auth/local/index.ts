'use strict';

import * as express from 'express';
import * as passport from 'passport';
import {signToken} from '../auth.service';
import * as Configs from "../../configurations";
let config:Configs.IServerConfigurations = Configs.getServerConfigs();
var ReCaptchaMiddleware = require('express-recaptcha-middleware')(config.recaptchaSecret);
let auth = require('../auth.service');
var nodemailer = require('nodemailer');
import * as cache from 'memory-cache';
import * as q from 'q';

let reCAPTCHA = require('recaptcha2');

let recaptcha = new reCAPTCHA({
  siteKey: config.recaptchaSite,
  secretKey: config.recaptchaSecret
});

var router = express.Router();

router.post('/', function(req, res, next) {
  let def = q.defer();
      let ip = req.connection.remoteAddress;
      let currentTries = +cache.get(ip) || 0;
      if (currentTries >= config.maxFailLoginTries) {
        recaptcha.validate(req.body.recaptcha)
        .then(function(){
          def.resolve();
        })
        .catch(function(errorCodes){
          def.reject({message: "Ошибка во вводе капчи!"}); // translate error codes to human readable text
        });
      } else {
        def.resolve();
      }
      def.promise.then(() => {
          passport.authenticate('local', function(err, user, info) {
          var error = err || info;
          if (error) {
            cache.put(ip, currentTries + 1, config.failLoginTriesSaveTime);
            return res.status(401).json(error);
          }
          if (!user) {
            cache.put(ip, currentTries + 1, config.failLoginTriesSaveTime);
            return res.status(404).json({message: 'Something went wrong, please try again.'});
          }
          cache.del(ip);
          var token = signToken(user._id, user.role, user.email, user.name);
          res.cookie('sso', token, {httpOnly:true, domain:'l24u.ru'});
          res.json({ token });
        })(req, res, next);
      }).catch((error) => {
        return res.status(500).json(error);
      });
});

router.post('/restore', ReCaptchaMiddleware('recaptcha'), require('./restore').restore);
if (process.env.NODE_ENV === 'dev') {
  router.post('/restoreDebug', require('./restore').restore);
}


export default router;