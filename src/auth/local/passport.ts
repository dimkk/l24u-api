import * as passport from 'passport';
import * as LocalStrategy from 'passport-local';
import * as q from 'q';

function localAuthenticate(User, email, password, done) {
  let userDef = q.defer();
  User.findOne({
    name: email.toLowerCase()
  })
  .exec()
  .then(user => {
      if (!user) {
        return done(null, false, {
                message: 'Такого аккаунта не существует.'
              });
      }
      user.authenticate(password, function(authError, authenticated) {
            if (authError) {
              return done(authError);
            }
            if (!authenticated) {
              return done(null, false, { message: 'Пароль не верный.' });
            } else {
              return done(null, user);
            }
          });
  });
}

export function setup(User/*, config*/) {
  passport.use(new LocalStrategy.Strategy({
    usernameField: 'email',
    passwordField: 'password' // this is the virtual field on the model
  }, function(email, password, done) {
    return localAuthenticate(User, email, password, done);
  }));
}
