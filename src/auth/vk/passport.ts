import * as passport from 'passport';
let VkStrategy = require('passport-vkontakte').Strategy;

export function setup(User, config) {
  passport.use(new VkStrategy({
    clientID: config.VK_ID,
    clientSecret: config.VK_SECRET,
    callbackURL: config.VK_CB
  },
  function(accessToken, refreshToken, profile, done) {
    User.findOne({'vk.id': profile.id}).exec()
      .then(user => {
        if(user) {
          return done(null, user);
        }

        user = new User({
          name: profile.displayName,
          email: profile.emails[0].value,
          role: 'user',
          username: profile.emails[0].value.split('@')[0],
          provider: 'vk',
          vk: profile._json
        });
        user.save()
          .then(savedUser => done(null, savedUser))
          .catch(err => done(err));
      })
      .catch(err => done(err));
  }));
}
