'use strict';

import * as express from 'express';
import * as passport from 'passport';
import {setTokenCookie} from '../auth.service';

var router = express.Router();

router
  .get('/', passport.authenticate('vkontakte', {
    failureRedirect: '/signup',
    scope: [
      'profile',
      'email'
    ],
    session: false
  }))
  .get('/callback', passport.authenticate('vkontakte', {
    failureRedirect: '/signup',
    session: false
  }), setTokenCookie);

export default router;
