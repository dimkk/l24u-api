import * as Configs from "./configurations";
import * as Mq from "./mqlistener";
import {CommunicationJob} from "./jobs/CommunicationJob";
import {RPCServer} from "./mqlistener";

console.log(`Running enviroment ${process.env.NODE_ENV || "dev"}`);

//Init Database
// const dbConfigs = Configs.getDatabaseConfig();
// const database = Database.init(dbConfigs);

//Starting Application Server
const serverConfigs = Configs.getServerConfigs();
//  const server = Server.init(serverConfigs); //, database);

// const job = new CommunicationJob("communicationJob");


// new RPCServer().start(job);

require('./app');