import * as restler from 'restler';
import * as Cfg from '../configurations';
import * as q from 'q';

let db:Cfg.IDb = Cfg.getCfg().get('db');

export class Auth {
    private static token:string;
    private static getToken() {
        let def = q.defer();
        restler.postJson(db.baseApi + '/api/l24users/login', {email: db.email, password:db.pass})
            .on('complete', function(data, response) {
                if (response.statusCode === 200) {
                    this.token = data.id;
                    def.resolve(data.id);
                } else {
                    console.error(data.error);
                    def.reject(data.error);
                }
            });
        return def.promise;
    }
    static authQuery() {
        let def = q.defer();
        if (true) {//!this.token) {
            this.getToken()
                .then((tok) => {
                    def.resolve({ query: {access_token: tok} });
                }).catch((err) => {
                    console.error(err);
                    def.reject(err);
                })
        }
        return def.promise;
    }
}