import * as restler from 'restler';
import * as Cfg from '../configurations';
import { Auth } from './auth';
import * as q from 'q';

let db:Cfg.IDb = Cfg.getCfg().get('db');

export class Accounts {
    static getAccounts() {
        let def = q.defer();
        Auth.authQuery().then((opts) => {
            restler.get(db.baseApi + '/api/Accounts', opts)
            .on('complete', function(data, response) {
                if (response.statusCode === 200) {
                    def.resolve(data);
                } else {
                    console.error(data.error);
                    def.reject(data.error);
                }
            });
        });
        return def.promise;
    }

    static createAccount(acc, ip) {
        let account = {
            login: acc.name,
            password: acc.password,
            lastAccess:Math.round(new Date().getTime() / 1000),
            accessLevel:0,
            banExpire:0,
            allowIp: '',
            allowHwid: '',
            bonus: '',
            bonusExpire: '',
            lastServer: 1,
            botReportPoints: 7,
            points:0
        };
        let def = q.defer();
        Auth.authQuery().then((opts) => {
            restler.postJson(db.baseApi + '/api/Accounts', account, opts)
            .on('complete', function(data, response) {
                if (response.statusCode === 200) {
                    def.resolve(data);
                } else {
                    console.error(data.error);
                    def.reject(data.error);
                }
            });
        });
        return def.promise;
    }

    static patchAccount(acc, id) {
        let def = q.defer();
        acc.secondarypassword = "";
        if (acc.login) delete acc.login;
        Auth.authQuery().then((opts) => {
            restler.patchJson(db.baseApi + '/api/Accounts/' + id, acc, opts)
            .on('complete', function(data, response) {
                if (response.statusCode === 200) {
                    def.resolve(data);
                } else {
                    console.error(data.error);
                    def.reject(data.error);
                }
            });
        });
        return def.promise;
    }
}

export interface IAccount {
    id?: number;
    login: string;
    password: string;
    lastIp?: string;
    lastAccess?: number;
}

export interface IDbAccount extends IAccount {
    accessLevel?: number;
    banExpire?: number;
    allowIp?: string;
    allowHwid?: string;
    bonus?: number;
    bonusExpire?: number;
    lastServer?: number;
    lastIp?: string;
    lastAccess?: number;
    botReportPoints?: number;
    points?: number;
    secondarypassword?: string;
}