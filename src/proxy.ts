'use strict';

import { Router } from 'express';
import * as Cfg from './configurations';
let config = Cfg.getServerConfigs();
let https = require('https');
import * as _ from 'lodash';
let request = require('request');

var router = Router();
router.get('/*', (req, res) => {
  let path = req.path.replace('/rapi', '');

  request('https://' + config.proxyAddress + path, function (error, response, body) {
    if (!error && response.statusCode === 200) {
      res.status(200).send(body);
    } else {
      res.status(response.statusCode).send(error);
    }
  });
});




module.exports = router;
