FROM mhart/alpine-node:6.9.1

MAINTAINER dimkk

# add files to container
ADD . /app

# specify the working directory
WORKDIR /app

RUN ls ./src/configurations/ \
    && npm install -g gulp \
    && npm install \
    && gulp build \
    && ls ./build/src/configurations/ 

ENV NODE_ENV=prod

EXPOSE 5000

# run application
CMD ["npm", "start"]